Some adjectives associated with following the rules rigidly are:

1. Conformist
2. Disciplined
3. Compliant
4. Obedient
5. Strict
6. Rule-abiding
7. Law-abiding
8. Unyielding
9. Inflexible
10. Rigorous
# Mon 10 Jun 16:12:21 CEST 2024 - what are some adjectives associated with following the rules rigidly?